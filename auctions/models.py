from django.db import models
from django.utils import timezone
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.db.models.signals import post_save

from rest_framework.authtoken.models import Token


class Auction(models.Model):
	title = models.CharField(max_length=100)
	description = models.CharField(max_length=1000, default='')
	start_price = models.FloatField()
	bid_step = models.FloatField()
	expiration_date = models.DateTimeField()
	owner = models.ForeignKey('auth.User', related_name='auctions', on_delete=models.CASCADE)

	def __str__(self):
		return self.title

	@property
	def expired(self):
		return self.expiration_date < timezone.now()

	def make_bid(self, user):
		if user == self.owner:
			raise ValueError('it\'s your auction. you can\'t raise it.')

		if self.expired:
			raise ValueError('auction is expired')

		# TODO: make sure bids are right ordered
		price = self.bids.last().price + self.bid_step
		bid = self.bids.create(price=price, user=user)

		return bid


class Bid(models.Model):
	user = models.ForeignKey('auth.User', related_name='bids', on_delete=models.CASCADE)
	auction = models.ForeignKey(Auction, related_name='bids', on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now_add=True)
	price = models.FloatField()

	def __str__(self):
		return '{}: {}'.format(self.user, self.price)


class ExpiredNotification(models.Model):
	date = models.DateTimeField('last checking time', auto_now_add=True)


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
	if created:
		Token.objects.create(user=instance)
