from django.utils import timezone

from celery import shared_task, task

from . import helpers
from . import models


@task
def check_expired_auctions():
	# pretty ugly and dumb, but it's the easiest way. I think.
	now = timezone.now()
	notification = models.ExpiredNotification.objects.last()

	if not notification:
		notification = models.ExpiredNotification.objects.create()

	expired_auctions = models.Auction.objects.filter(
		expiration_date__gt=notification.date,
		expiration_date__lte=now
	)

	for auction in expired_auctions:
		send_auction_expired.delay(auction.id)

	notification.date = now
	notification.save()


@shared_task
def send_auction_created(auction_id):
	helpers.send_auction_created(auction_id)


@shared_task
def send_bid_made(bid_id):
	helpers.send_bid_made(bid_id)


@shared_task
def send_auction_expired(auction_id):
	helpers.send_auction_expired(auction_id)


