from django.urls import path

from rest_framework.authtoken.views import obtain_auth_token

from . import views


urlpatterns = [
	path('auctions/', views.AuctionList.as_view(), name='auctions'),
	path('auctions/<int:pk>/', views.AuctionDetail.as_view(), name='auction_detail'),
	path('auctions/<int:pk>/bid/', views.AuctionMakeBid.as_view(), name='make_bid'),
	path('users/register/', views.UserCreate.as_view(), name='register_user'),
	path('users/get-token/', obtain_auth_token, name='get_token'),
]