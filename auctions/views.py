from django.utils import timezone
from django.contrib.auth.models import User

from rest_framework import status, generics, views as drf_views
from rest_framework import authentication, permissions
from rest_framework.response import Response

from . import tasks
from . import models
from . import serializers
from . import permissions as custom_permissions


class UserCreate(generics.CreateAPIView):
	queryset = User.objects.all()
	serializer_class = serializers.UserSerializer
	permission_classes = (permissions.AllowAny,)


class AuctionList(generics.ListCreateAPIView):
	serializer_class = serializers.AuctionSerializer
	authentication_classes = (authentication.TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)

	def get_queryset(self):
		queryset = models.Auction.objects.all()
		kind = self.request.query_params.get('filter', 'all').lower()

		if kind == 'expired':
			queryset = queryset.filter(expiration_date__lte=timezone.now())
		elif kind == 'active':
			queryset = queryset.filter(expiration_date__gt=timezone.now())

		return queryset

	def perform_create(self, serializer):
		auction = serializer.save(owner=self.request.user)

		tasks.send_auction_created.delay(auction.id)


class AuctionDetail(generics.RetrieveAPIView):
	queryset = models.Auction.objects.all()
	serializer_class = serializers.AuctionSerializer
	authentication_classes = (authentication.TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated, custom_permissions.IsOwnerOrReadOnly)


class AuctionMakeBid(drf_views.APIView):
	authentication_classes = (authentication.TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated, custom_permissions.IsOwnerOrReadOnly)

	# not sure should it be PATCH, POST or something else
	def patch(self, request, pk):
		try:
			auction = models.Auction.objects.get(pk=pk)
		except models.Auction.DoesNotExist:
			return Response(status=status.HTTP_404_NOT_FOUND)

		try:
			bid = auction.make_bid(request.user)

			tasks.send_bid_made.delay(bid.id)
		except ValueError as ex:
			response = Response({'error': str(ex)}, status=status.HTTP_400_BAD_REQUEST)
		else:
			response = Response(status=status.HTTP_200_OK)

		return response
