from django.contrib.auth.models import User

from rest_framework import serializers

from . import models


class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('username', 'email', 'password')
		extra_kwargs = {'password': {'write_only': True}}

	def create(self, validated_data):
		user = User(
			username=validated_data['username'],
			email=validated_data['email']
		)

		user.set_password(validated_data['password'])
		user.save()

		return user


class AuctionSerializer(serializers.ModelSerializer):
	bids = serializers.StringRelatedField(many=True, read_only=True)
	owner = serializers.ReadOnlyField(source='owner.username')

	class Meta:
		model = models.Auction
		fields = (
			'id', 'title', 'description', 'start_price', 'bid_step', 'expiration_date', 'bids', 'owner'
		)

	def create(self, validated_data):
		return models.Auction.objects.create(**validated_data)


class BidSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.Bid
		fields = read_only_fields = ('id', 'auction', 'date', 'price')
