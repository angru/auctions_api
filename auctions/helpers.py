from django.core.mail import send_mail, send_mass_mail

from . import models


def send_auction_created(auction_id):
	auction = models.Auction.objects.get(pk=auction_id)
	users = models.User.objects.all()

	# TODO: send_mass_mail
	send_mail(
		subject='Auction created: ${}: {}'.format(auction.start_price, auction.title),
		message=(
			'Hey dude, i\'m a new auction here. Hurry up and by me.\r\n{}'
			'\r\n\r\n Best regards, Auctions API'
		).format(auction.description),
		from_email='garton_maxolet@rambler.ru',
		recipient_list=[user.email for user in users],  # xxx: don't do this. even on DEV
		fail_silently=False
	)


def send_bid_made(bid_id):
	last_bid = models.Bid.objects.get(pk=bid_id)
	users = set(bid.user for bid in last_bid.auction.bids.all() if bid.user != last_bid.user)
	messages = (
		(
			'Hey dude, someone just beat your bid!',
			'Hurry up. Beat him back.\r\n{}\r\n\r\n Best regards, Auctions API'.format(last_bid.auction.title),
			'garton_maxolet@rambler.ru',
			[user.email],
		) for user in users
	)

	send_mass_mail(messages, fail_silently=False)


def send_auction_expired(auction_id):
	auction = models.Auction.objects.get(pk=auction_id)
	users = set(bid.user for bid in auction.bids.all())
	messages = (
		(
			'Hey dude, I\'m expired ((',
			'You will never ever be able to buy me again\r\n{}\r\n\r\n Best regards, Auctions API'.format(
				auction.description
			),
			'garton_maxolet@rambler.ru',
			[user.email]
		) for user in users
	)

	send_mass_mail(messages, fail_silently=False)
