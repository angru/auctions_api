# Generated by Django 2.0 on 2017-12-22 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auctions', '0009_remove_auction_bids_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExpiredNotification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now=True, verbose_name='last checking time')),
            ],
        ),
    ]
