amqp==2.2.2
billiard==3.5.0.3
celery==4.1.0
decorator==4.1.2
Django==2.0
django-extensions==1.9.8
djangorestframework==3.7.3
ipython==6.2.1
ipython-genutils==0.2.0
jedi==0.11.1
kombu==4.1.0
parso==0.1.1
pexpect==4.3.1
pickleshare==0.7.4
prompt-toolkit==1.0.15
ptyprocess==0.5.2
Pygments==2.2.0
pytz==2017.3
simplegeneric==0.8.1
six==1.11.0
traitlets==4.3.2
typing==3.6.2
vine==1.1.4
wcwidth==0.1.7
